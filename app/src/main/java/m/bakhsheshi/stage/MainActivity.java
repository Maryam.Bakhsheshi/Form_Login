package m.bakhsheshi.stage;

import android.annotation.SuppressLint;
import android.net.sip.SipAudioCall;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView name;
    TextView family;
    Button btnclick;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name=(TextView) findViewById(R.id.txtname);
        family=(TextView) findViewById(R.id.txtfamily);
        btnclick=(Button) findViewById(R.id.btnclick);
        result=(TextView)findViewById(R.id.result);

        btnclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username=name.getText().toString();
                String userfamily=family.getText().toString();
                result.setText(username + " " + userfamily);
                name.setText("");
                family.setText("");
                if(view.getId()==R.id.btnclick){
                    Toast.makeText(MainActivity.this,  "Your login was successful", Toast.LENGTH_SHORT).show();
                }

            }

        });



    }
}
